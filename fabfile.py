# -*- coding: utf-8 -*-

import os
from fabric.api import env, local, run, cd, lcd, sudo, warn_only, prompt

os.environ["__GEN_DOCS__"] = "1"

BUILD_DIR = "./public"

def docs_build(clear=None):
    """Build the documentation to temp dir"""
    #build_dir = "%s/docs_build" % TEMP_LOCAL
    #build_dir = "/home/opengeo/open-geotechnical.github.io/ogt-ags-py"
    # copy some stuff
    # local("cp ./static/images/favicon.* ./docs/_static/")
    # local("cp ./static/images/logo.* ./docs/_static/")
    # local("cp ./static/images/logo-small.* ./docs/_static/")

    ## run build
    clear_cache_flag = "-E" if clear else ""
    local("/usr/local/bin/sphinx-build %s -a  -b html ./docs/ %s" % (clear_cache_flag, BUILD_DIR))


def docs_server(port=8080):
    """Run local HTTP server with docs"""
    with lcd(BUILD_DIR):
        local("python -m SimpleHTTPServer %s" % port)